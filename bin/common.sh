# common shell functions

getconf() {
	perl -MYAML -e '
		($hashref, $arrayref, $string) = YAML::LoadFile($ARGV[0]);
		my $res=$hashref->{$ARGV[1]};
		die("Undefined value") unless defined $res;
		print $res,"\n"
		' /etc/dns-helpers.yaml "$1"
}
